package main

import (
	"angular-go-tutorial/handlers"
	"path"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.NoRoute(func(c *gin.Context) {
		dir, file := path.Split(c.Request.RequestURI)
		ext := filepath.Ext(file)
		if file == "" || ext == "" {
			c.File("./ui/dist/ui/index.html")
		} else {
			c.File("./ui/dist/ui/" + path.Join(dir, file))
		}
	})

	/*var c *gin.Context

	(*c).Header("Access-Control-Allow-Origin", "*")
	(*c).Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*c).Header("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	r.HandleContext(c)*/

	r.OPTIONS("/todo", handlers.SetupResponse)
	r.OPTIONS("/todo/:id", handlers.SetupResponse)
	r.GET("/todo", handlers.GetTodoListHandler)
	r.POST("/todo", handlers.AddTodoHandler)
	r.DELETE("/todo/:id", handlers.DeleteTodoHandler)
	r.PUT("/todo", handlers.CompleteTodoHandler)

	err := r.Run(":3000")
	if err != nil {
		panic(err)
	}
}
