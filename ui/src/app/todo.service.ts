import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Todo } from './todo';

const httpOptions = {
  /*headers: new HttpHeaders({
    'Access-Control-Allow-Origin':  '*',
    'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
    'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
  })*/
};

@Injectable()
export class TodoService {
  constructor(private httpClient: HttpClient) {}

  getTodoList() {
    return this.httpClient.get(environment.gateway + '/todo', httpOptions);
  }

  addTodo(todo: Todo) {
    return this.httpClient.post(environment.gateway + '/todo', todo, httpOptions);
  }

  completeTodo(todo: Todo) {
    return this.httpClient.put(environment.gateway + '/todo', todo, httpOptions);
  }

  deleteTodo(todo: Todo) {
    return this.httpClient.delete(environment.gateway + '/todo/' + todo.id, httpOptions);
  }
}
